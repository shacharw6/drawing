var pagesController = {

    pagesStack: [],

    init: function() {
        $('body > .page').hide();
        this.showPage("home");
    },

    showPage: function(pageId) {
        this.pagesStack.push({
            pageId: pageId
        });
        this._showPage(pageId);
    },

    _showPage: function(pageId) {
        $("body > .page").hide();
        $("body > #" + pageId).show();

        var controller = this._getController(pageId);
        if (controller && typeof controller.onEnter === "function") {
            controller.onEnter();
        }
    },

    _getController: function(pageId) {
        var controllerName = pageId + "Controller";
        return window[controllerName];
    },

    back: function() {
        if (this.pagesStack.length === 1) {
            return;
        }

        var prevPage = this.pagesStack.pop();
        var prevController = this._getController(prevPage.pageId);
        if (prevController && typeof prevController.onExit === "function") {
            prevController.onExit();
        }

        if (this.pagesStack.length) {
            var page = this.pagesStack[this.pagesStack.length - 1];
            this._showPage(page.pageI);
        }
    }
};