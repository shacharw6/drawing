var isMobile = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;

var app = {

    initialize: function() {
        if (isMobile) {
            document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        } else {
            this.onDeviceReady();
        }
    },

    onDeviceReady: function() {
        dbService.init().then(function(){
            homeController.init();

            pagesController.init();
        });
    }
};

app.initialize();

window.handleOpenURL = function (url) {
    setTimeout(function() {
        JSZipUtils.getBinaryContent(url, function(err, data) {
            if(err) {
                throw err;
            }
            JSZip.loadAsync(data)
                .then(function (zip) {
                    zip.file("data.json").async("string")
                        .then(function (project) {
                            project = JSON.parse(project);
                            dbService.saveProject(project).then(function(){
                                homeController.openProject(project);
                            });
                        });
                }, function (e) {

                });
        });
    }, 400);
};