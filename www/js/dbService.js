var dbService = {

    db: null,

    DB_NAME: "drawing.db",

    TABLES: {
        PROJECTS: 'projects'
    },

    init: function() {
        var that = this;
        return new Promise(function(resolve, reject){
            if (window.sqlitePlugin) {
                that.db = window.sqlitePlugin.openDatabase({name: that.DB_NAME, location: 'default'}, afterOpen);
            } else {
                that.db = window.openDatabase(that.DB_NAME, '1.0', that.DB_NAME, 2 * 1024 * 1024, afterOpen);
            }

            function afterOpen(db) {
                db.transaction(function(tx) {
                    that.createTables(tx);
                    resolve();
                }, function(err) {
                    console.log('Open database ERROR: ' + JSON.stringify(err));
                    reject();
                });
            }


        });
    },

    createTables: function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS ' + this.TABLES.PROJECTS + ' (id INTEGER PRIMARY KEY AUTOINCREMENT, json)');
    },

    saveProject: function(project) {
        return this.doQuery('INSERT INTO ' + this.TABLES.PROJECTS + ' (json) VALUES (?)', [JSON.stringify(project)]).then(function(res){
            project.id = res.insertId;
            return project
        });
    },

    getAllProjects: function() {
        var that = this;
        return this.doQuery('SELECT * FROM ' + this.TABLES.PROJECTS, []).then(function(res){
            var i,
                items = [],
                rows = res.rows;

            for (i = 0; i < rows.length; i++) {
                items.push(that.convertProjectRowJsonToObject(rows.item(i)));
            }

            return items;
        });
    },

    //for testing only
    deleteAllProjects: function() {
        return this.doQuery('DELETE FROM ' + this.TABLES.PROJECTS, []);
    },

    convertProjectRowJsonToObject: function(row) {
        var project = JSON.parse(row.json);
        project.id = row.id;
        return project;
    },

    doQuery: function(query, values) {
        var that = this;

        return new Promise(function(resolve, reject){
            that.db.transaction(function(tx) {
                tx.executeSql(query, values, function(tx, res) {
                    resolve(res);
                }, function(tx, err){
                    console.log(err.message);
                    reject(err);
                });
            });
        });
    }

    /*getProjectByName: function(name, success) {
        this.db.executeSql('SELECT * FROM ' + this.TABLES.PROJECTS + ' where name="' + name + '"', [], function(res) {
            success(res.rows.item(0));
        }, function() {

        });
    }*/

    /*updateProjectGraphics: function(id, graphics) {
     var thumbnail = document.getElementsByTagName("canvas")[0].toDataURL();
     this.db.executeSql("UPDATE " + this.TABLES.PROJECTS + " SET graphics ='" + graphics + "',thumbnail='" + thumbnail + "' WHERE id = " + id, [], function(s) {
     debugger;
     }, function(err) {
     debugger;
     });
     },*/
};