var homeController = {
    init: function() {
      this.$ = $('#home');
      this.$recentProjects = this.$.find(".recent-projects .projects");
      this.$allProjects = this.$.find(".all-projects .projects");
      this.renderProjects();
    },

    renderProjects: function(){
        var that = this;

        that.$recentProjects.empty();
        this.loadProjects().then(function(projects){
            projects.forEach(function(project){
                var $project = that.createProjectElem(project);
                that.$recentProjects.append($project);
            });
        });
    },

    createProjectElem: function(project) {
        return $("<div/>")
                    .addClass("project")
                    .on("click", this.openProject.bind(this, project))
                    .append(
                        $("<div/>").text(project.name)
                    )
    },

    openProject: function(project) {
        pagesController.showPage("projectId") //TODO add ID to page of a project?
        if (typeof project === "object") {
            project = JSON.stringify(project); //TODO remove stringify
        }

        PROJECT.load(project);
        var page = PROJECT.getCurrentPage(); //TODO?
        var pageDef = CONTROLS_DEF["page"]; //TODO?
        page.setMode(pageDef.MODES.RUNTIME); //TODO?
    },

    loadProjects: function() {
        var that = this;
        return dbService.getAllProjects().then(function(projects){
            if (projects.length === 0) {
                return that.loadDummyProject();
            } else {
                return projects;
            }
        });
    },

    loadDummyProject: function() {
        var dummyProject = {type: "BLANK", name: "עבודה 1", data: 1509781910351, pages: [{settings: {backgroundColor: "#fff"}, type: "page", controls: [{settings: {fontSize: "22px", color: "#ff0000", text: "שחר"}, type: "input", position: {x: 189, y: 30}}]}]};
        return dbService.saveProject(dummyProject).then(function(project){
            return [project];
        });
    }
};
