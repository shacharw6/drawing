/**
 * Created by i048697 on 25/10/2017.
 */
((function () {

    var defaultSettings = {

        color: "blue",
        brushWidth: 2,
        data: "{}",
        width: 300,
        height: 150
    }


    function canvas () {

        this.brushColor = 'red';
        this.brushWidth = 1;
        this.$elm = $("<div class='canvas'>");
        var $colors = $("<div  class='colors'>");
        var $red = $("<div class='color red' data-color='red'>");
        var $green = $("<div class='color green' data-color='green'>");
        var $blue = $("<div class='color blue' data-color='blue'>");
        $colors.append([$red, $green, $blue]);


        $colors.on('click', function (e) {
            if ($(e.srcElement).hasClass('color')) {
                this.brushColor = $(e.srcElement).attr('data-color');
            }
        }.bind(this));

        // this.$canvas =  $("<canvas width='100%' height='100%'>");
        this.setSettings(this.getDefaultSettings());
        //this.$elm.append(this.$canvas);
        this.$elm.append($colors);


    }


    canvas.prototype = new BASE_CONTROL;
    canvas.prototype.constructor = canvas;
    canvas.prototype.getMarkup = function () {
        return this.$elm;
    }
    canvas.prototype.getResizableDirections = function () {
        return RESIZE_DIRECTION.BOTH;
    }
    canvas.prototype.getEditorSettings = function () {
        return {
            brushWidth: {type: "text", title: "Brush Width"},
            color: {type: "text", title: "Color"},
            text: {type: "text", title: "Value"},
            alignment: {type: "text", title: "Alignment"}
        };
    }


    canvas.prototype.getDefaultSettings = function () {
        return defaultSettings;
    }

    canvas.prototype.afterSizeChanged = function () {


        this.createNewCanvas(this.settings);
    }


    canvas.prototype.applySettings = function (settings) {

        this.brushWidth = settings.brushWidth;
        this.createNewCanvas(settings);
        // this.$canvas.css("color", settings.color);
        // this.$canvas.css("font-size", settings.fontSize);
        // this.$canvas.val(settings.text);
    }

    canvas.prototype.createNewCanvas = function (settings) {
        graphic.clearPaths();
        var self = this;
        var container = this.$elm[0];

        this.$elm.find("canvas").remove();


        Sketch.create({
            width: settings.width,
            height: settings.height,
            lastPos: {x: 0, y: 0},
            fullscreen: false,
            container: container,
            autoclear: false,

            setup: function () {
                graphic.context = this;
                graphic.initGraphicsData(JSON.parse(settings.data));

            },

            // Event handlers

            keydown: function () {
                if (this.keys.C) {
                    this.clear();
                }
            },

            touchstart: function () {

                this.lastPos = this.touches[0];

                graphic.createPath(self.brushColor, self.brushWidth);

            },

            touchmove: function () {
                if (graphic.currentPath) {
                    this.lastPos = this.touches[0];

                    for (var i = this.touches.length - 1, touch; i >= 0; i--) {
                        touch = this.touches[i];
                        graphic.drawLine({startX: touch.ox, startY: touch.oy, endX: touch.x, endY: touch.y});
                    }
                }

            },

            touchend: function () {


                if (graphic.currentPath) {

                    graphic.closePath();
                    settings.data = graphic.toJSON();
                    //DBService.updateDocumentGraphics(window.currentProject.id, graphic.toJSON());

                }
            }
        });


    }


    CONTROLS_DEF["canvas"] = canvas;
})())
