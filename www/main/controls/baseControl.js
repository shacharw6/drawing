/**
 * Created by i048697 on 25/10/2017.
 */
var CONTROLS_DEF = {};
var RESIZE_DIRECTION = {
    NONE: "none",
    BOTH: "both",
    HEIGHT: "height",
    WIDTH: "width"
}
var BASE_CONTROL = (function () {
    function baseControl () {
        this.settings = {};
    }

    baseControl.prototype.position = {x: 0, y: 0};
    baseControl.prototype.getMarkup = function () {

    }


    baseControl.prototype.getName = function () {
        return this.constructor.name;
    }

    baseControl.prototype.updateWidth = function (w) {
        this.settings.width = w;
    }

    baseControl.prototype.afterSizeChanged = function () {

    }


    baseControl.prototype.updateHeight = function (h) {
        this.settings.height = h;

    }
    baseControl.prototype.buildEditors = function (settings) {
        var $editor = $("<table cellpadding='0'  cellspacing='0'>");
        $.each(settings, function (k, v) {
            var editorSettings = this.getEditorSettings()[k];

            if (editorSettings) {
                var editorDef = EDITOR_DEF[editorSettings.type];
                var e = new editorDef({value: v, title: editorSettings.title});
                var editor = e.getEditor()
                e.onChange(function () {
                    settings[k] = e.getFormattedValue();
                    this.setSettings(settings);
                }.bind(this));
                $editor.append(editor);
            }
        }.bind(this));
        return $editor;
    }
    baseControl.prototype.getPosition = function () {
        return this.position;
    }

    baseControl.prototype.setPosition = function (pos) {
         this.position = pos;
    }

    baseControl.prototype.getEditor = function (settings) {
        return this.buildEditors(this.settings);
    }

    baseControl.prototype.getEditorSettings = function () {
        return this.editors;
    }

    baseControl.prototype.createID = function () {
        return "ctrl_" + new Date().getTime();
    },

        baseControl.prototype.getDefaultSettings = function () {
            return {};
        }

    baseControl.prototype.setSettings = function (settings) {
        settings = settings || {};
        this.settings = $.extend({}, this.getDefaultSettings(), settings, true);

        this.applySettings(this.settings);


    }
    baseControl.prototype.getSettings = function () {
        return this.settings;

    }
    baseControl.prototype.getSettingsEditors = function () {

    }
    baseControl.prototype.getResizableDirections = function () {
        return RESIZE_DIRECTION.NONE;
    }

    baseControl.prototype.applySettings = function () {

    }
    return baseControl;
}());