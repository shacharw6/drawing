/**
 * Created by i048697 on 25/10/2017.
 */
/**
 * Created by i048697 on 25/10/2017.
 */
((function () {

    var defaultSettings = {
        backgroundImage: "",
        backgroundColor: "#fff"
    }

    var draggable = $("<div class='draggable'>");


    function page () {
        this.$page = $("<div class='page contextual'></div>");

        this.controls = [];
        this.context = null;
        this.setMode("design");
    }

    page.prototype = new BASE_CONTROL;


    page.prototype.constructor = page;

    page.prototype.setMode = function (mode) {

        if (mode === page.MODES.RUNTIME && this.pageMode !== page.MODES.RUNTIME) {
            this.$page.off("mousedown");
            this.$page.find(".movable").off("mousedown");
        } else if (mode === page.MODES.DESIGN && this.pageMode !== page.MODES.DESIGN) {
            this.$page.on("mousedown", function (e) {
                if (e.srcElement) {
                    var $c = $(e.srcElement);
                    if ($c.hasClass("contextual")) {
                        this.setContext($c);
                    }
                }
            }.bind(this));
            this.$page.find(".movable").on("mousedown", function (e) {
                var $movable = $(e.srcElement);
                if ($movable.hasClass("movable")) {
                    this.startMove(e, e.srcElement);
                }

            }.bind(this));
        }


        this.pageMode = mode;
    }
    page.prototype.getMarkup = function () {
        return this.$page;
    }
    page.prototype.getDefaultSettings = function () {
        return defaultSettings;
    }

    page.prototype.getResizableDirections = function () {
        return RESIZE_DIRECTION.NONE;
    }

    page.prototype.applySettings = function (settings) {

        this.$page.css("background-image", 'url(' + settings.backgroundImage + ')');
        this.$page.css("background-color", settings.backgroundColor);
    }
    page.prototype.getEditorSettings = function () {
        return {
            backgroundImage: {type: "image", title: "Background Image"},
            backgroundColor: {type: "color", title: "Background Color"}
        };
    }


    var moveOffset = {x: 0, y: 0};


    page.prototype.setContext = function ($lem) {

        if ($lem.hasClass("contextual")) {
            if (this.context) {
                this.context.removeClass("selected");
            }
            this.context = $lem;
            var edt = this.getControl($lem).getEditor();
            $(".toolbar-control-editor").html("").append(edt);
            this.context.addClass("selected");
            jscolor.installByClassName("jscolor");
        }
    }

    page.prototype.getControl = function (m) {


        var ctrlId = m.attr("data-ctrl");
        return this.controls[ctrlId] || this;

    }

    page.prototype.startDrag = function (item) {
        var $body = $("body");
        draggable.empty();

        draggable.append($(item).find(".icon").clone());
        $body.append(draggable.data("control", $(item).data("control")));
        $body.on("mousemove", this.drag.bind(this));
        $body.on("mouseup", this.stopDrag.bind(this));

    }

    page.prototype.drag = function (e) {

        draggable.css("left", e.clientX + "px")
        draggable.css("top", e.clientY + $("html").scrollTop() + "px")

        var rect = this.$page[0].getBoundingClientRect();
        if (this.contains(rect, e.clientX, e.clientY)) {
            this.$page.addClass("hover");
        } else {
            this.$page.removeClass("hover");
        }
        e.preventDefault();
        return false;
    }

    page.prototype.getData = function () {
        var pageData = {};
        pageData['settings'] = this.getSettings();
        pageData['type'] = this.getName();
        var controls = [];
        for (var ctrlId in this.controls) {
            var control = this.controls[ctrlId];
            var ctrlData = {};
            ctrlData['settings'] = control.getSettings();
            ctrlData['type'] = control.getName();
            //var movablePosition = control.getMarkup().parent().position();
            ctrlData["position"] = control.getPosition(); //{x: movablePosition.left, y: movablePosition.top};
            controls.push(ctrlData);
        }
        ;
        pageData["controls"] = controls;
        return pageData;
    }

    page.prototype.load = function (pageData) {

        // for (var c in this.controls) {
        //     this.controls
        // }

        this.setSettings(pageData.settings);
        for (var ctrlId in pageData.controls) {
            var controlData = pageData.controls[ctrlId];
            var c = this.createControl(controlData.type, controlData.position || {x: 0, y: 0});
            c.setSettings(controlData.settings);
        }

    };

    page.prototype.contains = function (rect, x, y) {
        return rect.x <= x && x <= rect.x + rect.width &&
            rect.y <= y && y <= rect.y + rect.height;
    }

    page.prototype.startMove = function (e, movable) {
        var $movable = $(movable);

        if (e.button === 2) {
            return;
        }
        $movable.detach();
        var mode = "move";
        this.$page.append($movable);
        var rect = $movable[0].getBoundingClientRect();
        var ctrl = this.getControl($movable);
        var dir = ctrl.getResizableDirections();

        if (dir === RESIZE_DIRECTION.BOTH) {
            if (e.layerX >= rect.width - 5 && e.layerY >= rect.height - 5) {
                mode = "resize";
            }
        } else if (dir === RESIZE_DIRECTION.HEIGHT) {
            if (e.layerY >= rect.height - 5) {
                mode = "resize";
            }
        } else if (dir === RESIZE_DIRECTION.WIDTH) {
            if (e.layerX >= rect.width - 5) {
                mode = "resize";
            }
        }
        moveOffset = {x: e.layerX, y: e.layerY};
        var $body = $("body");
        $body.on("mousemove", this.move.bind({page: this, ctx: movable, mode: mode}))
        $body.on("mouseup", this.stopMove.bind(this));


    }

    page.prototype.move = function (e) {
        var movable = $(this.ctx);
        if (this.mode === "move") {
            movable.css("left", e.clientX - this.page.$page.offset().left - moveOffset.x + "px")
            movable.css("top", e.clientY - this.page.$page.offset().top - moveOffset.y + $("html").scrollTop() + "px")
        } else {
            var w = e.clientX - (movable.offset().left  ), h = e.clientY + $("html").scrollTop() - (movable.offset().top );
            var ctrl = this.page.getControl(movable);
            var dir = ctrl.getResizableDirections();
            var sizeChanged = false;
            if (dir === RESIZE_DIRECTION.BOTH || dir === RESIZE_DIRECTION.HEIGHT) {
                movable.css("height", h + "px");
                ctrl.updateHeight(h);
                sizeChanged = true;
            }
            if (dir === RESIZE_DIRECTION.BOTH || dir === RESIZE_DIRECTION.WIDTH) {
                movable.css("width", w + "px");
                ctrl.updateWidth(w);
                sizeChanged = true;
            }
            if (sizeChanged) {
                ctrl.afterSizeChanged();
            }


        }
        e.preventDefault();
        return false;
    }

    page.prototype.stopMove = function (e) {
        var context = PROJECT.getCurrentPage().getContext();
        var ctrl = this.getControl(context)

        var pos = context.position();
        ctrl.setPosition({x: pos.left, y: pos.top});

        var $body = $("body");
        $body.off("mousemove");
        $body.off("mouseup");

    }
    page.prototype.getContext = function () {
        return this.context;
    }

    page.prototype.delete = function () {

        var _id = this.context.attr("data-ctrl");
        delete  this.controls[_id];
        this.context.remove();
    };
    page.prototype.createControl = function (ctrl, position) {

        var movable = $("<div>");
        var ctrl = new CONTROLS_DEF[ctrl];
        var ctrlId = "ctrl_" + new Date().getTime();
        this.controls[ctrlId] = ctrl;
        var movable = $("<div tabindex='-1' class='movable contextual removable'>").append(ctrl.getMarkup());

        if (ctrl.getResizableDirections() !== RESIZE_DIRECTION.NONE) {

            movable.addClass("resizable " + ctrl.getResizableDirections());
        }
        ctrl.setPosition(position);
        movable.attr("data-ctrl", ctrlId);
        this.$page.append(movable);
        this.setContext(movable);
        movable.css("left", position.x + "px")
        movable.css("top", position.y + "px")


        movable.on("mousedown", function (e) {
            var $movable = $(e.srcElement);
            if ($movable.hasClass("movable")) {
                this.startMove(e, e.srcElement);
            }

        }.bind(this));
        return ctrl;
    }


    page.prototype.stopDrag = function (e) {
        var $body = $("body");
        $body.off("mousemove");
        $body.off("mouseup");
        var ctrl = draggable.data("control");

        draggable.css("left", "")
        draggable.css("top", "")
        this.$page.removeClass("hover");
        draggable.remove();
        var rect = this.$page[0].getBoundingClientRect();
        if (this.contains(rect, e.clientX, e.clientY)) {
            this.createControl(ctrl, {x: e.clientX - this.$page.offset().left, y: e.clientY - this.$page.offset().top});
        }
    }
    page.MODES = {DESIGN: "design", RUNTIME: "runtime"};
    CONTROLS_DEF["page"] = page;
})())