/**
 * Created by i048697 on 25/10/2017.
 */
/**
 * Created by i048697 on 25/10/2017.
 */
((function () {

    var defaultSettings = {
        fontSize: "16px",
        color: "#333",
        text: "Text"
    }



    function text () {
        this.$elm = $("<div>");
        this.$text = $("<div style='white-space:pre-wrap;overflow:hidden;font-size:" + defaultSettings.fontSize + ";color: " + defaultSettings.color + "'>").text(defaultSettings.text);
        this.$elm.append(this.$text);
        this.setSettings(this.getDefaultSettings());
    }



    text.prototype = new BASE_CONTROL;
    text.prototype.constructor = text;
    text.prototype.getMarkup = function () {
        return this.$elm;
    }
    text.prototype.getDefaultSettings = function () {
        return defaultSettings;
    }

    text.prototype.getResizableDirections = function () {
        return RESIZE_DIRECTION.NONE;
    }

    text.prototype.applySettings = function (settings) {

        this.$text.css("color", settings.color);
        this.$text.css("font-size", settings.fontSize);
        this.$text.text(settings.text);
    }
    text.prototype.getEditorSettings = function () {
        return {
            fontSize: {type: "text", title: "Font size"},
            color: {type: "color", title: "Color"},
            text: {type: "textarea", title: "Value"}
        };
    }


    CONTROLS_DEF["text"] = text;
})())