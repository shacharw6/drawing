/**
 * Created by i048697 on 25/10/2017.
 */
((function () {

    var defaultSettings = {
        options: "",
        color: "#333",
        value: "",
        fontSize: "18px"
    }


    function select () {
        this.$elm = $("<div>");
        this.$select = $("<select style='width:100%;box-sizing:border-box;color: " + defaultSettings.color + "'>");
        this.$elm.append(this.$select);

        this.setSettings(this.getDefaultSettings());
        this.$select.on("change", function () {
            this.settings.value = this.$select.val();
            this.applySettings(this.settings);
        }.bind(this));
    }


    select.prototype = new BASE_CONTROL;
    select.prototype.constructor = select;
    select.prototype.getMarkup = function () {
        return this.$elm;
    }
    select.prototype.getResizableDirections = function () {
        return RESIZE_DIRECTION.WIDTH;
    }
    select.prototype.getEditorSettings = function () {
        return {
            options: {type: "text", title: "Options"},
            fontSize: {type: "text", title: "Font size"},
            color: {type: "color", title: "Color"},
            value: {type: "text", title: "Value"}
        };
    }

    select.prototype.afterSizeChanged = function () {
        this.$elm.css("width", this.settings.width || "auto");
    }


    select.prototype.getDefaultSettings = function () {
        return defaultSettings;
    }

    select.prototype.applySettings = function (settings) {
        var options = this.settings.options.split(";");
        this.$elm.find("option").remove();
        $.each(options, function (k, v) {
            this.$select.append($('<option>' + v + '</option>'))
        }.bind(this));
        this.$elm.css("width", this.settings.width || "auto");
        this.$select.css("color", settings.color);
        this.$select.css("font-size", settings.fontSize);
        this.$select.val(settings.value);

    }


    CONTROLS_DEF["select"] = select;
})())
