/**
 * Created by i048697 on 30/10/2017.
 */
PDFJS.disableWorker = false; // due to CORS
var PROJECT = {
    TYPES: {BLANK: "BLANK", PDF: "PDF", IMAGE: "IMAGE"},
    pages: [],
    currentPage: -1,
    name: "",
    type: "",
    numberOfPages: 0,
    $page: null,
    date: new Date(),
    reset: function () {
        this.pages = [];
        this.currentPage = -1;

        this.numberOfPages = 0;
        $('.thumbnails').empty();
    },
    create: function (name, type, file) {
        this.reset();
        this.name = name;
        if (type === PROJECT.TYPES.BLANK) {
            var newPage = new CONTROLS_DEF["page"];
            newPage.setSettings({});
            this.pages.push(newPage);
            this.numberOfPages = 1;
            this.currentPage = 0;
            this.moveToPage(0);
        } else if (type === PROJECT.TYPES.PDF) {

            var fileReader = new FileReader();

            fileReader.onload = function () {

                //Step 4:turn array buffer into typed array
                var typedarray = new Uint8Array(this.result);

                PROJECT.loadPDF(typedarray, function (num) {
                    $('.thumbnails img').on('click',function () {
                        PROJECT.moveToPage($(this).index());
                    });
                    PROJECT.numberOfPages = num;
                    PROJECT.moveToPage(0);
                });
            };
            fileReader.readAsArrayBuffer(file)

        }
        else if (type === PROJECT.TYPES.IMAGE) {

        }
    },
    moveToPage: function (pageNum) {

        if (pageNum < this.numberOfPages) {
            var newPage = PROJECT.pages[pageNum];
            var page = newPage.getMarkup();
            if (this.$page) {
                this.$page.remove();
            }
            this.$page = page;
            $("body").append(page);
            this.currentPage = pageNum;
        }

    },
    load: function (json) {
        this.reset();
        var p = JSON.parse(json);
        PROJECT.type = p.type;
        PROJECT.name = p.name;
        PROJECT.numberOfPages = p.pages.length;

        for (var i = 0; i < PROJECT.numberOfPages; i++) {
            var page = new CONTROLS_DEF["page"];
            page.load(p.pages[i]);
            PROJECT.pages.push(page);
            $('.thumbnails').append($('<img>').attr('src',page.settings.tumbnail));
        }
        PROJECT.moveToPage(0);
        $('.thumbnails img').on('click',function () {
            PROJECT.moveToPage($(this).index());
        });
    },

    loadPDF: function (dataArray, onDone) {

        var currentPage = 1;
        var scale = 3;


        PDFJS.getDocument(dataArray).then(function (pdf) {
            getPage();

            function getPage () {
                pdf.getPage(currentPage).then(function (page) {

                    var viewport = page.getViewport(scale);
                    var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
                    var renderContext = {canvasContext: ctx, viewport: viewport};

                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    page.render(renderContext).then(function () {
                        var src = canvas.toDataURL('image/jpeg', 0.9);
                        var tumbnail = canvas.toDataURL('image/jpeg', 0.0002);
                        var page = new CONTROLS_DEF["page"];
                        page.setSettings({backgroundImage: src,tumbnail:tumbnail});
                        $('.thumbnails').append($('<img>').attr('src',page.settings.tumbnail));
                        PROJECT.pages.push(page);
                        if (currentPage < pdf.numPages) {
                            currentPage++;

                            getPage();

                        } else {
                            onDone(pdf.numPages);
                        }
                    });

                });
            }
        });
    },

    save: function () {
        var project = {type: PROJECT.type, name: PROJECT.name, data: (new Date().getTime()), pages: []};
        for (var i = 0; i < PROJECT.pages.length; i++) {
            project.pages.push(PROJECT.pages[i].getData());
        }
        var blob = new Blob([JSON.stringify(project)], {type: "text/plain;charset=utf-8"});
        var zip = new JSZip();
        zip.file("data.json", blob, {base64: true});
        zip.generateAsync({
            type: "blob",
            compression: "DEFLATE",
            compressionOptions: {
                level: 9
            }
        }).then(function (content) {
            // see FileSaver.js
            saveAs(content, PROJECT.name + ".wpg");
        });
    },
    getCurrentPage: function () {
        var cp = this.pages[this.currentPage];
        if (cp) {
            return cp;
        }
        var emptyPage = new CONTROLS_DEF["page"];
        return emptyPage;
    }
}