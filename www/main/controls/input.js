/**
 * Created by i048697 on 25/10/2017.
 */
((function () {

    var defaultSettings = {
        fontSize: "16px",
        color: "#333",
        text: ""
    }


    function input () {
        this.$elm = $("<div>");
        this.$input = $("<input value='" + defaultSettings.text + "' style='font-size:" + defaultSettings.fontSize + ";width:100%;box-sizing:border-box;color: " + defaultSettings.color + "'>");
        this.$elm.append(this.$input);
        this.setSettings(this.getDefaultSettings());
        this.$input.on("change", function () {
            this.settings.text = this.$input.val();
            this.applySettings(this.settings);
        }.bind(this));
    }


    input.prototype = new BASE_CONTROL;
    input.prototype.constructor = input;
    input.prototype.getMarkup = function () {
        return this.$elm;
    }
    input.prototype.getResizableDirections = function () {
        return RESIZE_DIRECTION.WIDTH;
    }
    input.prototype.getEditorSettings = function () {
        return {
            fontSize: {type: "text", title: "Font size"},
            color: {type: "color", title: "Color"},
            text: {type: "text", title: "Value"}
        };
    }

    input.prototype.afterSizeChanged = function () {
        this.$elm.css("width", this.settings.width|| "auto");
    }


    input.prototype.getDefaultSettings = function () {
        return defaultSettings;
    }

    input.prototype.applySettings = function (settings) {
        this.$elm.css("width", this.settings.width|| "auto");
        this.$input.css("color", settings.color);
        this.$input.css("font-size", settings.fontSize);
        this.$input.val(settings.text);
    }


    CONTROLS_DEF["input"] = input;
})())
