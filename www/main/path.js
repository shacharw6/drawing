/**
 * Created by i048697 on 03/04/2016.
 */
var path = function path(_id) {
	this.id = _id || "path_" + new Date().getTime();
	this.elements = [];
	this.color = null;
	this.rect = {x1: 1e6, x2: 0, y1: 1e6, y2: 0};

}

path.prototype.addLine = function(info) {
	this.elements.push({info: info, type: "line"});
	this.rect.x1 = Math.min(info.startX, info.endX, this.rect.x1);
	this.rect.x2 = Math.max(info.startX, info.endX, this.rect.x2);
	this.rect.y1 = Math.min(info.startY, info.endY, this.rect.y1);
	this.rect.y2 = Math.max(info.startY, info.endY, this.rect.y2);

}

path.prototype.addText = function(info) {
	this.elements.push({info: info, type: "text"});
	this.rect.x1 = info.x;
	this.rect.y1 = info.y;
	this.text = info.text;

}

path.prototype.draw = function() {
	for (var i = 0; i < this.elements.length; i++) {
		var elm = this.elements[i];
		var path;
		if (elm.type === 'text') {
			path = new text(elm.info);
		} else {
			path = new line(elm.info);
		}

		path.draw();
	}
}

path.prototype.delete = function() {
	delete  graphic.paths[this.id];
	graphic.drawGraphicsData();
},

	path.prototype.detect = function(x, y) {

		var rect = this.rect;
		if (x > rect.x1 && x < rect.x2 && y > rect.y1 && y < rect.y2) {

			var centerPos = {
				x: rect.x1 + Math.abs(rect.x1 - rect.x2),
				y: rect.y1 + Math.abs(rect.y1 - rect.y2)
			};
			return Math.sqrt((x - centerPos.x) * (x - centerPos.x) + (y - centerPos.y) * (y - centerPos.y ));
		}
		return -1;
	}

path.prototype.toString = function() {
	return JSON.stringify(this.elements);
}