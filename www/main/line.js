/**
 * Created by i048697 on 03/04/2016.
 */

CanvasRenderingContext2D.prototype.wrapText = function (text, x, y, lineHeight) {

	var lines = text.split("\n");

	for (i = 0; i < lines.length; i++) {
		this.fillText(lines[i],x, y+ (i*lineHeight));
	}

};

var line = function line(info) {
	this.info = info;
	this.type = graphic.ELEMENTS_TYPES.LINE;
}

line.prototype.draw = function() {
	var info = this.info;
	var context = graphic.context;
	context.lineCap = 'round';
	context.lineJoin = 'round';
	context.strokeStyle = graphic.currentPath.color;
	context.lineWidth = graphic.currentPath.lineWidth;

	context.beginPath();
	context.moveTo(info.startX, info.startY);
	context.lineTo(info.endX, info.endY);
	context.stroke();

}


var text = function text(info) {
	this.info = info;
	this.type = graphic.ELEMENTS_TYPES.TEXT;
}

text.prototype.draw = function() {
	var info = this.info;
	var context = graphic.context;
	context.font = TEXT_SIZES[graphic.currentPath.lineWidth - 1] + " Arial";
	context.fillStyle = graphic.currentPath.color;
	context.wrapText(info.text,info.x,info.y,info.lineHeight );
	//context.fillText(info.text, info.x, info.y);

}


//line.prototype.detect = function(x, y){
//
//        var imageData = graphic.context.getImageData(0, 0,  graphic.context.width,  graphic.context.height),
//
//            inputData = imageData.data,
//            pData = (~~x + (~~y *  graphic.context.width)) * 4;
//
//        if (inputData[pData + 3]) {
//            return true;
//        }
//        return false;
//}
