var selectedItem = {
	_path: null,
	setPath: function(path) {
		this._path = path;
		this._draw();
	},
	drag: function() {

	},
	drop: function() {

	},
	_draw: function() {
		if (this._path) {
			var context = graphic.context;
			context.lineCap = 'round';
			context.lineJoin = 'round';
			context.fillStyle = '';
			context.lineWidth = SKETCH_GLOBAL.size;

			var r = this._path.rect;
			context.beginPath();
			context.rect(r.x1, r.y1, Math.abs(r.x1 - r.x2), Math.abs(r.y1 - r.y2));
			context.stroke();
			graphic.closePath();
		}
	},
	clear: function() {

	}
};


var isOpenText = false;
var graphic = {
	paths: {},
	context: null,
	currentPath: null,
	ELEMENTS_TYPES: {
		LINE: "line",
		TEXT: 'text'
	},

	createPath: function(color, radius) {
		this.currentPath = new path();
		this.currentPath.color = color;
		this.currentPath.lineWidth = radius;
	},

	closePath: function() {
		this.paths[this.currentPath.id] = this.currentPath;
	   	this.currentPath = null;
	},
	drawText: function(info) {
		graphic.currentPath.addText(info);
		var newText = new text(info);
		newText.draw();

	},

	addInputText: function(info) {



		isOpenText = false;
        var $inputCover = $(".input-cover-div");
		$inputCover.show();
		var $input = $(".canvas-text-input");

		$input.css('height', '0').css('height', $input[0].scrollHeight);
		$input.css('width', '0').css('width', $input[0].scrollWidth);
		$input.css({
			top: info.y,
			left: info.x,
			height:$input[0].scrollHeight,
			width:$input[0].scrollWidth,
			color: graphic.currentPath.color,
			fontSize: TEXT_SIZES[graphic.currentPath.lineWidth - 1]
		}).data("top",info.y).focus();


	//	var first = 0;
		//window.setTimeout(function() {
		//	$(window).off("tap",this.closeTextArea);
		setTimeout(function(){
			isOpenText = true;
		}.bind(this),200);
	},
	//closeTextArea:function(e){
	//	var $input = $(".canvas-text-input");
	//
	//		if (e.target !== $input[0] && $input.is(":focus")) {
	//			$input.blur();
	//		}else
	//		{
	//			$(window).one("tap",this.closeTextArea.bind(this));
	//		}
	//
	//
	//},
	drawLine: function(info) {
		var newline = new line(info);
		newline.draw();
		graphic.currentPath.addLine(info);
	},
	initWithImage: function(imgData, callback) {
		//A4 ration
		this.context.clear();

		if(imgData && imgData!=='undefined') {
			var img = new Image();

			img.addEventListener("load", function() {
				var width = 580;
				var ratio = 210 / 297;//A4 Ratio
				var height = width / ratio;
				graphic.context.drawImage(img, 0, 0, width, height);
				callback();
			}, false);
			img.src = imgData;
		} else
		{
			callback();
		}
	},

	initGraphicsData: function(gd) {
		var g;

		this.paths = {};
		for (g in gd) {
			var gData = gd[g];
			this.paths[gData.id] = new path(gData.id);

			this.paths[gData.id].elements = gData.elements;
			this.paths[gData.id].color = gData.color;
			this.paths[gData.id].lineWidth = gData.lineWidth;
			this.paths[gData.id].rect = gData.rect;

		}

		this.drawGraphicsData();
	},

	clearPaths: function() {

		this.paths = {};
		this.currentPath = null;
	},

	drawGraphicsData: function() {

		var p;
		for (p in this.paths) {

			this.currentPath = this.paths[p];
			this.paths[p].draw();

		}

	},

	toJSON: function() {
		return JSON.stringify(graphic.paths);
	},
	getHitPath: function(x, y) {
		var p,
			minLength,
			minPath;
		for (p in this.paths) {
			var l = this.paths[p].detect(x, y);
			if (l != -1) {
				if (!minLength || l < minLength) {
					minLength = l;
					minPath = this.paths[p];

				}
			}
		}

		return minPath;
	}
}



