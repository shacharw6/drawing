/**
 * Created by i048697 on 25/10/2017.
 */


/**
 * Created by i048697 on 25/10/2017.
 */
var EDITOR_DEF = {};
var BASE_EDITOR = (function () {
    function baseEditor () {
        this.settings = {};

    }

    function baseEditor (params) {
        params = params||{};
        this.value = params.value;
        this.$editor = "<td>Title</td><td>Value</td>"
    }

    baseEditor.prototype.setValue = function (value) {
        this._parse(value);
    };

    baseEditor.prototype.isValid = function () {
        return true;
    };

    baseEditor.prototype.setOptions = function (flags) {
        this.flags = flags;
    };
    baseEditor.prototype.onChange = function (callback) {
        this.onChangeCallBack = callback;
    }

    baseEditor.prototype.getFormattedValue = function () {
        return "";
    };
    baseEditor.prototype._parse = function (value) {

        this.data = value;
    };
    baseEditor.prototype.getEditor = function () {
        return this.$editor
    };
    baseEditor.prototype.setTitle = function (title) {
        this.title = title;
    };

    baseEditor.prototype.init = function () {

    };

    return baseEditor;
}());