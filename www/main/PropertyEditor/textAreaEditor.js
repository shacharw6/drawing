/**
 * Created by i048697 on 25/10/2017.
 */


/**
 * Created by i048697 on 25/10/2017.
 */

(function () {


    function textEditor (params) {
        this.value = params.value;
        this.title = params.title;
        this.$elm = $("<textarea>");
        this.$editor = $("<tr>");

        this.$elm.on("keyup", function () {
           var val =  this.$elm.val()
            this.setValue(val);
            if(this.onChangeCallBack) {
                this.onChangeCallBack(val);
            }
        }.bind(this));
    }
    textEditor.prototype = new BASE_EDITOR;

    textEditor.prototype.setValue = function (value) {
        this._parse(value);
    };

    textEditor.prototype.isValid = function () {
        return true;
    };

    textEditor.prototype.setOptions = function (flags) {
        this.flags = flags;
    };


    textEditor.prototype.getFormattedValue = function () {
        return this.data;
    };
    textEditor.prototype._parse = function (value) {

        this.data = value;
    };
    textEditor.prototype.getEditor = function () {

        this.$editor.append($("<td>").text(this.title));
        this.$elm.val(this.value);
        this.$editor.append($("<td>").append(this.$elm));
        return this.$editor
    };


    textEditor.prototype.init = function () {

    };

    EDITOR_DEF["textarea"] = textEditor;
}());