/**
 * Created by i048697 on 25/10/2017.
 */


/**
 * Created by i048697 on 25/10/2017.
 */

(function () {


    function textEditor (params) {
        this.value = params.value;
        this.title = params.title;


        this.$elm = $("<input type='file'>");
        this.$previewImage = $("<img height='200' alt='Image preview...'>");

        this.$editor = $("<tr>");

        this.$elm.on("change", function () {
            this.preview();


        }.bind(this));
    }


    textEditor.prototype = new BASE_EDITOR;
    textEditor.prototype.preview = function () {
        var reader = new FileReader();
        var $previewImage = this.$previewImage;
        reader.addEventListener("load", function () {
            $previewImage.attr("src", reader.result);
            var val =reader.result;
            this.setValue(val);
            if (this.onChangeCallBack) {
                this.onChangeCallBack(val);
            }
        }.bind(this), false);
        var file = this.$elm[0].files[0];
        if (file) {
            reader.readAsDataURL(file);
        }
    }
    textEditor.prototype.setValue = function (value) {
        this._parse(value);
    };

    textEditor.prototype.isValid = function () {
        return true;
    };

    textEditor.prototype.setOptions = function (flags) {
        this.flags = flags;
    };


    textEditor.prototype.getFormattedValue = function () {
        return this.data;
    };
    textEditor.prototype._parse = function (value) {

        this.data = value;
    };
    textEditor.prototype.getEditor = function () {

        this.$editor.append($("<td>").text(this.title));
        this.$previewImage.attr("src", this.value);
        this.$editor.append($("<td>").append(this.$elm).append(this.$previewImage));
        return this.$editor
    };


    textEditor.prototype.init = function () {

    };

    EDITOR_DEF["image"] = textEditor;
}());